import javafx.application.Application
import view.App

class AppEntry {
    companion object {
        @JvmStatic
        fun main(args : Array<String>) {
            Application.launch(App::class.java)
        }
    }
}