package model

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import logic.Euro
import logic.MoneyEntry
import logic.Month
import org.koin.core.component.KoinApiExtension

@OptIn(KoinApiExtension::class)
class MonthModel(private val month: Month) {
    val moneyEntries : ObservableList<MoneyEntry> = FXCollections.observableArrayList()
    val wantedSumProp : ObjectProperty<Euro> = SimpleObjectProperty()
    val neededSumProp : ObjectProperty<Euro> = SimpleObjectProperty()
    val investmentSumProp : ObjectProperty<Euro> = SimpleObjectProperty()
    val monthPresentationProp : StringProperty = SimpleStringProperty()

    init {
        month.listeners.add(object: Month.Listener {
            override fun entriesUpdated() {
                moneyEntries.clear()
                month.entries.forEach { moneyEntries.add(it) }
                setSums()
            }
        })
        monthPresentationProp.set(month.toString())
        setSums()
    }

    private fun setSums() {
        wantedSumProp.set(month.getWantedSpending())
        neededSumProp.set(month.getNeededSpending())
        investmentSumProp.set(month.getInvestmentSpending())
    }

    fun addMoneyEntry(me : MoneyEntry) {
        month.update(me)
    }
}