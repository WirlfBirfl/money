package model

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import logic.*
import util.Left
import util.Right
import java.io.File
import java.time.LocalDate

class MoneyTrackerModel {
    private val moneyTrackerProp : ObjectProperty<MoneyTracker> = SimpleObjectProperty<MoneyTracker>()
    val nameProp : StringProperty = SimpleStringProperty()
    var state = State.EMPTY
    val listeners = mutableListOf<Listener>()
    var months : MutableList<Month> = mutableListOf()

    init {
        moneyTrackerProp.addListener{
            _, _, n -> if(n != null) listeners.forEach{it.trackerInitialized()}
        }
    }

    fun new() {
        moneyTrackerProp.set(MoneyTracker())
        moneyTrackerProp.get()?.name = "Finanzen"
        initFromMt()
        state = State.DIRTY
    }

    private fun initFromMt() {
        nameProp.set(moneyTrackerProp.get().name)
        calcMonths()
    }

    fun load(location : File) : Boolean {
        val text = location.readText()
        when(val tmp = fromJSON(text)) {
            is Left<String> -> println("Error")
            is Right<MoneyTracker> -> {
                moneyTrackerProp.set(tmp.success)
                initFromMt()
                state = State.SAVED
            }
        }
        return true
    }

    fun save(location : File) : Boolean{
        val json = toJSON(moneyTrackerProp.get())
        location.writeText(json)
        state = State.SAVED
        return true
    }

    enum class State {
        EMPTY, SAVED, DIRTY
    }

    fun addMoneyEntry(desc : String, date : LocalDate, value : Euro, type : EntryType, groupId : Int = 0) {
        val mt = moneyTrackerProp.get() ?: return
        val newEntry = mt.addMoneyEntry(desc, date, value, type, groupId)
        val monthToUpdate = months.firstOrNull { it.month == date.monthValue && it.year == date.year }
        if(monthToUpdate == null) {
            val m = Month(date.year, date.monthValue, mutableListOf(newEntry))
            months.add(m)
            months.sortBy { it }
        } else {
            monthToUpdate.update(newEntry)
        }
        listeners.forEach { it.valueAdded() }
        state = State.DIRTY
    }

    fun getMonthModels() : List<MonthModel> =
            if(moneyTrackerProp.get() != null) {
                val m = ArrayList<MonthModel>()
                months.forEach { m.add(MonthModel(it)) }
                m
            }else
                listOf()

    private fun calcMonths() {
        val mt = moneyTrackerProp.get() ?: return
        this.months.clear()
        months.addAll(mt.getMonths())
    }

    interface Listener {
        fun valueAdded()

        fun trackerInitialized()
    }
}