import model.MoneyTrackerModel
import org.koin.dsl.module
import view.*

val main = module {
    // data model
    single { MoneyTrackerModel().apply { new() } }

    // main view
    single { MainView() }

    // view models
    single { HistoryTabViewModel(get()) }
    factory { NewEntryPaneViewModel(get()) }
    single { MainViewModel(get()) }
    single { MoneyEntryTableViewModel(get()) }
}