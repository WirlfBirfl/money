package logic

import java.time.LocalDate
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList

class MoneyTracker {
    var name : String
    val location : String?
    val entries : MutableList<MoneyEntryGroup>
    val categories : MutableList<String>
    init {
        entries = ArrayList()
        categories = ArrayList()
        location = null
        name = ""
    }

    fun getMonths() : List<Month> = entries
        .map { it.moneyEntries }
        .flatten()
        .groupBy {
            Pair(it.date.monthValue, it.date.year)
        }.map {
            val (month, year) = it.key
            Month(year, month, it.value.sortedBy { me -> me.date }.toMutableList())
        }.sortedWith(object: Comparator<Month> {
            override fun compare(o1: Month?, o2: Month?): Int {
                if(o1 == null) return 1
                if(o2 == null) return -1
                if(o1.year == o2.year) return o2.month - o1.month
                return o2.year - o1.year
            }
        })

    fun addMoneyEntry(desc : String, date : LocalDate, value : Euro, type : EntryType, groupId : Int = 0) : MoneyEntry{
        val me = MoneyEntry(desc, date, value, type, groupId)
        if(groupId <= 0)
            entries.add(MoneyEntryGroup(entries.size + 1).also { it.moneyEntries.add(me) })
        else
            getMoneyEntryGroup(groupId).ifPresent{it.moneyEntries.add(me)}
        return me
    }

    private fun getMoneyEntryGroup(id : Int) =
            if(id > 0 && id <= entries.size) Optional.of(entries[id - 1]) else Optional.empty()
}

class Euro(val euro : Int, val cent : Int) {
    override fun toString(): String {
        return "$euro.${cent.leftPad()}€"
    }

    operator fun plus(other : Euro) : Euro {
        val cents = cent + other.cent
        val centsToEuro = cents as Int / 100
        val actualCents = cents % 100
        return Euro(euro = other.euro + euro + centsToEuro, cent = actualCents)
    }
}

fun Int.leftPad() : String {
    if(this < 10) return "0$this"
    return "$this"
}

enum class EntryType {
    INCOME, WANT, NEED, INVESTMENT;

    override fun toString() =
        when(this) {
            INCOME -> "Einkommen"
            WANT -> "Ausgabe"
            NEED -> "Essentiell"
            INVESTMENT -> "Investieren"
        }
}