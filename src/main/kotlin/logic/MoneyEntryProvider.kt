package logic

import java.time.LocalDate

sealed class MoneyEntryProvider(moneyEntry: MoneyEntry, val end : LocalDate?) {
    val exceptionDates : MutableList<LocalDate> = mutableListOf()
    var id : Int = -1

    abstract fun update(now : LocalDate) : MoneyEntry?

    protected abstract fun calculateNextEntry(me : MoneyEntry) : MoneyEntry?

    fun addExceptionDate(ld : LocalDate) = exceptionDates.add(ld)

    abstract fun changeValue(value : Euro)
}

class MonthlyProvider(
        private val monthInterval : Array<Long>,
        moneyEntry: MoneyEntry,
        end : LocalDate?)
    : MoneyEntryProvider(moneyEntry, end) {

    private var intervalIndex = 0
    private var nextEntry : MoneyEntry?

    override fun update(now : LocalDate): MoneyEntry? {
        val ne = nextEntry ?: return null
        if(!ne.date.isAfter(now)) {
            nextEntry = calculateNextEntry(ne)
            return if(exceptionDates.contains(nextEntry?.date)) null else ne
        }
        return null
    }

    init {
        nextEntry = calculateNextEntry(moneyEntry)
    }

    override fun calculateNextEntry(me : MoneyEntry) : MoneyEntry? {
        val nextDate = me.date.plusMonths(monthInterval[intervalIndex])
        intervalIndex = (intervalIndex + 1) % monthInterval.size
        return  if(end != null && nextDate.isAfter(end))
                    null
                else
                    MoneyEntry(me.description, nextDate, me.value, me.type, me.groupId, id)
    }

    override fun changeValue(value: Euro) {
        val base = nextEntry ?: return
        nextEntry = MoneyEntry(base.description, base.date, value, base.type, base.groupId, id)
    }
}