package logic

import util.reduceOrDefault

class Month(val year: Int, val month: Int, val entries : MutableList<MoneyEntry>) : Comparable<Month>{
    val listeners : MutableList<Listener> = ArrayList()

    override fun toString()=
        when(month) {
            1 -> "Januar $year"
            2 -> "Februar $year"
            3 -> "März $year"
            4 -> "April $year"
            5 -> "Mai $year"
            6 -> "Juni $year"
            7 -> "Juli $year"
            8 -> "August $year"
            9 -> "September $year"
            10 -> "Oktober $year"
            11 -> "November $year"
            12 -> "Dezember $year"
            else -> "Huh?"
        }

    fun update(me : MoneyEntry) {
        entries.add(me)
        entries.sortedBy { it }
        listeners.forEach { it.entriesUpdated() }
    }

    fun getWantedSpending() = sumByType(EntryType.WANT)

    fun getNeededSpending() = sumByType(EntryType.NEED)

    fun getInvestmentSpending() = sumByType(EntryType.INVESTMENT)

    private fun sumByType(et : EntryType) : Euro = entries
        .filter { it.type == et }
        .map{ it.value }
        .reduceOrDefault(Euro(0,0)) { acc, euro -> acc + euro }

    override fun compareTo(other: Month): Int {
        if(other == null) return -1
        if(year == other.year) return other.month - month
        return other.year - year
    }

    interface Listener {
        fun entriesUpdated()
    }
}
