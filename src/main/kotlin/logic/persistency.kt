package logic

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import util.Either
import util.Left
import util.Right

fun toJSON(moneyTracker : MoneyTracker) : String {
    val gson = GsonBuilder().setPrettyPrinting().create()
    return gson.toJson(moneyTracker)
}

fun fromJSON(json : String) : Either<String, MoneyTracker> {
    val gson = Gson()
    var mt : MoneyTracker? = null
    return try {
        mt = gson.fromJson(json, MoneyTracker::class.java)
        Right(mt)
    } catch (e : JsonSyntaxException) {
        Left("Not a valid JSON file")
    }
}