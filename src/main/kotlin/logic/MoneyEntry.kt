package logic

import java.time.LocalDate

class MoneyEntryGroup(val id : Int) {
    val moneyEntries : MutableList<MoneyEntry> = ArrayList()
    private val moneyEntryProviders : MutableList<MoneyEntryProvider> = ArrayList()

    fun addMoneyEntryProvider(mep : MoneyEntryProvider) {
        mep.id = moneyEntryProviders.size
        moneyEntryProviders.add(mep)
    }
}

class MoneyEntry (
        val description : String,
        val date : LocalDate,
        val value : Euro,
        val type : EntryType,
        val groupId : Int,
        val providerId : Int = -1
) : Comparable<MoneyEntry> {
    val kategories : MutableList<String>

    init {
        kategories = ArrayList<String>()
    }

    override fun compareTo(other: MoneyEntry): Int {
        return date.compareTo(other.date)
    }
}