package util

sealed class Either<out Err,out Suc>
data class Left<Err>(val error : Err) : Either<Err, Nothing>()
data class Right<Suc>(val success : Suc) : Either<Nothing, Suc>()