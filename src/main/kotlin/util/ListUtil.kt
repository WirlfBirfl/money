package util

fun <T> List<T>.reduceOrDefault(defaultValue : T, op : (T, T) -> T) =
        if(this.isEmpty()) defaultValue else this.reduce(op)