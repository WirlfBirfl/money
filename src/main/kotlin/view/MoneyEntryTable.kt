package view

import javafx.beans.property.ObjectProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.Scene
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.GridPane
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.stage.StageStyle
import logic.EntryType
import logic.Euro
import logic.MoneyEntry
import model.MoneyTrackerModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.net.URL
import java.time.LocalDate
import java.util.*

@OptIn(KoinApiExtension::class)
class MoneyEntryTableView : AnchorPane(), KoinComponent{
    val LAYOUT_FILE = "tableentryview.fxml"
    @FXML
    var entryTableView : TableView<MoneyEntry>? = null
    @FXML
    var dateCol : TableColumn<MoneyEntry, LocalDate>? = null
    @FXML
    var valueCol : TableColumn<MoneyEntry, Euro>? = null
    @FXML
    var descCol : TableColumn<MoneyEntry, String>? = null
    @FXML
    var typeCol : TableColumn<MoneyEntry, EntryType>? = null
    @FXML
    var catCol : TableColumn<MoneyEntry, List<String>>? = null
    private val viewModel : MoneyEntryTableViewModel by inject()

    init {
        FXMLHelper().fxmlLoad<AnchorPane>(LAYOUT_FILE, this, this)
        dateCol!!.prefWidthProperty().bind(entryTableView!!.widthProperty().multiply(0.15))
        valueCol!!.prefWidthProperty().bind(entryTableView!!.widthProperty().multiply(0.15))
        descCol!!.prefWidthProperty().bind(entryTableView!!.widthProperty().multiply(0.25))
        typeCol!!.prefWidthProperty().bind(entryTableView!!.widthProperty().multiply(0.15))
        catCol!!.prefWidthProperty().bind(entryTableView!!.widthProperty().multiply(0.35))
        dateCol?.setCellValueFactory { SimpleObjectProperty(it.value.date) }
        valueCol?.setCellValueFactory { SimpleObjectProperty(it.value.value) }
        descCol?.setCellValueFactory { SimpleStringProperty(it.value.description) }
        typeCol?.setCellValueFactory { SimpleObjectProperty(it.value.type) }

        entryTableView?.items = viewModel.tableEntries
    }

    @FXML
    fun addNewEntry(e : ActionEvent) {
        val stage = Stage()
        stage.initStyle(StageStyle.UNDECORATED)
        stage.scene = Scene(NewEntryPane())
        stage.initModality(Modality.WINDOW_MODAL)
        stage.showAndWait()
    }
}

class MoneyEntryTableViewModel(private val historyTabViewModel: HistoryTabViewModel) {
    var tableEntries : ObservableList<MoneyEntry> = historyTabViewModel.selectedMonthEntries
}