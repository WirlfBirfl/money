package view

import javafx.fxml.FXMLLoader

class FXMLHelper {
    fun <T>fxmlLoad(file : String, root : Any, controller : Any) {
        val loader = FXMLLoader()
        loader.classLoader = this::class.java.classLoader
        loader.setControllerFactory { it}
        loader.setController(controller)
        loader.setRoot(root)
        loader.load<T>(root::class.java.classLoader.getResourceAsStream(file))
    }
}