package view

import javafx.application.Platform
import javafx.beans.property.*
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.ButtonType
import javafx.scene.control.MenuBar
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.BorderPane
import javafx.stage.FileChooser
import javafx.stage.Stage
import model.MoneyTrackerModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.io.File

@OptIn(KoinApiExtension::class)
class MainView : BorderPane(), KoinComponent {
    private val layoutFile = "mainview.fxml"
    @FXML
    var displayPane : AnchorPane? = null
    @FXML
    var menuBar : MenuBar? = null
    @FXML
    var historyTab : HistoryTab? = null
    private val viewModel : MainViewModel by inject()

    init {
        FXMLHelper().fxmlLoad<BorderPane>(layoutFile, this, this)
        this.sceneProperty().addListener {
            _, _, newScene -> newScene.windowProperty().addListener {
                _, _, newWindow ->
               val n = (newWindow as Stage)
               n.titleProperty().bind(viewModel.nameProp)
               n.setOnCloseRequest { closeAction() }
            }
        }
    }

    private fun wantToSave() : Boolean? {
        val alert = Alert(Alert.AlertType.CONFIRMATION)
        val save = ButtonType("Speichern")
        val dismiss = ButtonType("Verwerfen")
        val cancel = ButtonType("Abbrechen")
        alert.title = "Schließen ohne zu speichern?"
        alert.buttonTypes.clear()
        alert.headerText = "Schließen ohne zu Speichern?"
        alert.buttonTypes.addAll(save, dismiss, cancel)
        val res = alert.showAndWait()
        if(res.isPresent) {
            val type = res.get()
            if(type == cancel) {
                return null
            }
            if(type == dismiss)
                return false
            if(type == save)
                return true
        }
        return null
    }

    private fun closeAction() {
        val res = viewModel.closable()
        if(!res) {
            val wantToSave = wantToSave() ?: return
            if(wantToSave) {
                val file = createFile() ?: return
                viewModel.save(file)
            }
        }
        Platform.exit()
    }

    @FXML
    fun onMenuClose(e : ActionEvent) {
        closeAction()
    }

    @FXML
    fun onMenuOpen(e : ActionEvent) {
        val res = viewModel.loadable()
        if(!res) {
            val wantToSave = wantToSave() ?: return
            if(wantToSave) {
                val file = createFile() ?: return
                viewModel.save(file)
            }
        }
        val fileLoaded = openFile() ?: return
        viewModel.setLocation(fileLoaded)
        viewModel.load()
    }

    @FXML
    fun onMenuNew(e : ActionEvent) {
        val res = viewModel.creatable()
        if(!res) {
            val wantToSave = wantToSave() ?: return
            if(wantToSave) {
                val file = createFile() ?: return
                viewModel.save(file)
            }
        }
        viewModel.new()
    }

    @FXML
    fun onMenuSave(e : ActionEvent) {
        if(viewModel.savable()) {
            viewModel.save()
        } else {
            val file = createFile() ?: return
            viewModel.save(file)
        }
    }

    @FXML
    fun onMenuSaveAs(e : ActionEvent) {
        val file = createFile() ?: return
        viewModel.save(file)
    }

    private fun createFile() : File? {
        val fc = FileChooser()
        fc.extensionFilters.add(FileChooser.ExtensionFilter("Money tracker files", "*.mt"))
        return fc.showSaveDialog(displayPane!!.scene.window)
    }

    private fun openFile() : File? {
        val fc = FileChooser()
        fc.extensionFilters.add(FileChooser.ExtensionFilter("Money tracker files", "*.mt"))
        return fc.showOpenDialog(displayPane!!.scene.window)
    }
}

class MainViewModel(private val mtm : MoneyTrackerModel) {
    val nameProp : StringProperty = mtm.nameProp
    private val changesProp : BooleanProperty = SimpleBooleanProperty(false)
    private val fileProp : ObjectProperty<File?> = SimpleObjectProperty(null)

    init {
        mtm.listeners.add(object: MoneyTrackerModel.Listener {
            override fun valueAdded() {
                changesProp.set(true)
            }

            override fun trackerInitialized() {
            }
        })
    }

    fun save() {
        if(changesProp.get()) {
            val loc = fileProp.get()
            if(loc != null) {
                mtm.save(loc)
            }
        }
    }

    fun save(file : File) {
        mtm.save(file)
        fileProp.set(file)
    }

    fun setLocation(file : File) {
        fileProp.set(file)
    }

    fun new() {
        fileProp.set(null)
        changesProp.set(false)
        mtm.new()
    }

    fun load() {
        val f = fileProp.get() ?: return
        mtm.load(f)
    }

    fun closable() : Boolean {
        val location = fileProp.get() ?: return !changesProp.get()
        if(changesProp.get()) {
            mtm.save(location)
        }
        return true
    }

    fun loadable() : Boolean = closable()

    fun creatable() : Boolean = closable()

    fun savable() : Boolean = fileProp.get() != null
}