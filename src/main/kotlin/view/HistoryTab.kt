package view

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.fxml.FXML
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.layout.GridPane
import javafx.util.Callback
import logic.MoneyEntry
import model.MoneyTrackerModel
import model.MonthModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@OptIn(KoinApiExtension::class)
class HistoryTab: GridPane(), KoinComponent {
    private val layoutFile = "historytab.fxml"
    private val viewModel : HistoryTabViewModel by inject()
    @FXML
    var moneyEntryTableView : MoneyEntryTableView? = null
    @FXML
    var monthListView : ListView<MonthModel>? = null

    init {
        FXMLHelper().fxmlLoad<GridPane>(layoutFile, this, this)
        monthListView?.items = viewModel.months
        monthListView?.selectionModel?.selectedItemProperty()?.addListener { _,_, month ->
            if(month != null)
                viewModel.select(month)
        }
        monthListView?.cellFactory = Callback<ListView<MonthModel>, ListCell<MonthModel>> {
            object: ListCell<MonthModel>() {
                override fun updateItem(item: MonthModel?, empty: Boolean) {
                    super.updateItem(item, empty)
                    val mm = item ?: return
                    text = "${mm.monthPresentationProp.get()} ${mm.neededSumProp.get()}"
                }
            }
        }
    }
}

@OptIn(KoinApiExtension::class)
class HistoryTabViewModel(private val mtm : MoneyTrackerModel) : KoinComponent {
    val months : ObservableList<MonthModel> = FXCollections.observableArrayList()
    val selectedMonthEntries : ObservableList<MoneyEntry> = FXCollections.observableArrayList()

    init {
        mtm.listeners.add(object: MoneyTrackerModel.Listener {
            override fun valueAdded() {
                initialize()
            }

            override fun trackerInitialized() {
                initialize()
            }
        })
        initialize()
    }

    private fun initialize() {
        months.clear()
        months.addAll(mtm.getMonthModels())
        selectedMonthEntries.clear()
        if(months.size > 0) selectedMonthEntries.addAll(months[0].moneyEntries)
    }

    fun select(month: MonthModel) {
        selectedMonthEntries.clear()
        selectedMonthEntries.addAll(month.moneyEntries)
    }
}