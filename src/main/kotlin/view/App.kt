package view

import javafx.application.Application
import javafx.application.Platform
import javafx.scene.Scene
import javafx.stage.Stage
import main
import org.koin.core.context.startKoin

class App : Application(){
    override fun start(primaryStage: Stage?) {
        startKoin {
            modules(main)
        }
        if (primaryStage != null) {
            val mv = MainView()
            primaryStage.scene = Scene(mv)
            primaryStage.show()
        }
        else {
            println("Wow, something went terribly wrong")
            Platform.exit()
        }
    }
}