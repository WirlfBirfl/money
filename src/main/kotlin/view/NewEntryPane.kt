package view

import javafx.beans.property.*
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ComboBox
import javafx.scene.control.DatePicker
import javafx.scene.control.TextField
import javafx.scene.layout.GridPane
import javafx.stage.Stage
import logic.EntryType
import logic.Euro
import logic.Month
import model.MoneyTrackerModel
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.time.LocalDate

@OptIn(KoinApiExtension::class)
class NewEntryPane() : GridPane(), KoinComponent{
    private val LAYOUT_FILE = "newentrypane.fxml"
    val viewModel : NewEntryPaneViewModel by inject()
    @FXML
    var descTextField : TextField? = null
    @FXML
    var valueTextField : TextField? = null
    @FXML
    var datePicker : DatePicker? = null
    @FXML
    var addButton : Button? = null
    @FXML
    var typeComboBox : ComboBox<EntryType>? = null

    init {
        FXMLHelper().fxmlLoad<GridPane>(LAYOUT_FILE, this, this)
        viewModel.closeRequested.addListener{
            _, _, new -> if(new) (scene.window as Stage).close()
        }
        addButton?.disableProperty()?.bind(viewModel.addable.not())
        viewModel.descriptionProp.bind(descTextField?.textProperty())
        viewModel.valueStringProp.bind(valueTextField?.textProperty())
        viewModel.dateProp.bind(datePicker?.valueProperty())
        viewModel.typeProp.bind(typeComboBox?.valueProperty())
        typeComboBox?.items?.addAll(EntryType.INCOME, EntryType.INVESTMENT, EntryType.NEED, EntryType.WANT)
        typeComboBox?.selectionModel?.selectFirst()
    }

    @FXML
    fun onCancel(e : ActionEvent) {
        viewModel.cancel()
    }

    @FXML
    fun onAdd( e : ActionEvent) {
        viewModel.add()
    }
}

@OptIn(KoinApiExtension::class)
class NewEntryPaneViewModel(private val mtm : MoneyTrackerModel) : KoinComponent {
    val closeRequested : BooleanProperty = SimpleBooleanProperty(false)
    val addable : BooleanProperty = SimpleBooleanProperty(false)
    val dateProp : ObjectProperty<LocalDate> = SimpleObjectProperty()
    val descriptionProp : StringProperty = SimpleStringProperty()
    val valueStringProp : StringProperty = SimpleStringProperty()
    val typeProp : ObjectProperty<EntryType> = SimpleObjectProperty()
    val validValueProp : BooleanProperty = SimpleBooleanProperty(true)
    val valueProp : ObjectProperty<Euro> = SimpleObjectProperty(null)
    val valueRegex : Regex = Regex("-?\\d+(.\\d{1,2})?")

    init {
        addable.bind(dateProp.isNotNull.and(descriptionProp.isNotEmpty).and(validValueProp).and(typeProp.isNotNull))
        valueStringProp.addListener {
            _, _, n ->
            val mr = valueRegex.matchEntire(n)
            if(mr != null) {
                validValueProp.set(true)
                val x = n.split(".")
                if(x.size == 2) {
                    valueProp.set(Euro(x[0].toInt(), if(x[1].length == 1) x[1].toInt() * 10 else x[1].toInt()))
                } else if (x.size == 1) {
                    valueProp.set(Euro(x[0].toInt(), 0))
                }
            } else {
                validValueProp.set(false)
                valueProp.set(null)
            }
        }
    }

    fun cancel() {
        closeRequested.set(true)
    }

    fun add() {
        mtm.addMoneyEntry(descriptionProp.get(), dateProp.get(), valueProp.get(), typeProp.get())
        closeRequested.set(true)
    }
}