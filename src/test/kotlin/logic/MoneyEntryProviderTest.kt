package logic

import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.Month

internal class MonthlyProviderTest {

    @Test
    fun `update method should provide a result for each month expected`() {
        // given
        val baseEntry = MoneyEntry(
                "test desc",
                LocalDate.of(2020, 5, 1),
                Euro(10,0U),
                EntryType.NEED,
                1
        )
        val interval = arrayOf(1L)
        val endDate = LocalDate.of(2020, 10, 1)
        val now = LocalDate.of(2020, 10, 2)

        // when
        val mp = MonthlyProvider(interval, baseEntry, endDate)
        val entries = mutableListOf<MoneyEntry>()
        var tmp: MoneyEntry?
        while(true) {
            tmp = mp.update(now)
            if(tmp == null) break
            entries.add(tmp)
        }

        // then
        assert(entries.size == 5)
        assert(entries[0].date.month == Month.JUNE)
        assert(entries[1].date.month == Month.JULY)
        assert(entries[2].date.month == Month.AUGUST)
        assert(entries[3].date.month == Month.SEPTEMBER)
        assert(entries[4].date.month == Month.OCTOBER)
    }

    @Test
    fun `update method should provide a result for each month expected with changed value`() {
        // given
        val baseEntry = MoneyEntry(
                "test desc",
                LocalDate.of(2020, 5, 1),
                Euro(10,0U),
                EntryType.NEED,
                1
        )
        val interval = arrayOf(1L)
        val endDate = LocalDate.of(2020, 10, 1)
        val now = LocalDate.of(2020, 10, 2)

        // when
        val mp = MonthlyProvider(interval, baseEntry, endDate)
        val entries = mutableListOf<MoneyEntry>()
        var tmp: MoneyEntry?
        var index = 0
        val value = Euro(12, 0U)
        while(true) {
            if(index == 2) {
                mp.changeValue(value)
            }
            tmp = mp.update(now)
            if(tmp == null) break
            entries.add(tmp)
            index++
        }

        // then
        assert(entries.size == 5)
        assert(entries[0].date.month == Month.JUNE)
        assert(entries[0].value != value)
        assert(entries[1].date.month == Month.JULY)
        assert(entries[1].value != value)
        assert(entries[2].date.month == Month.AUGUST)
        assert(entries[2].value == value)
        assert(entries[3].date.month == Month.SEPTEMBER)
        assert(entries[3].value == value)
        assert(entries[4].date.month == Month.OCTOBER)
        assert(entries[4].value == value)
    }

    @Test
    fun `update method should provide a result for each month expected with the given interval`() {
        // given
        val baseEntry = MoneyEntry(
            "test desc",
            LocalDate.of(2020, 5, 1),
            Euro(10,0U),
            EntryType.NEED,
            1
        )
        val interval = arrayOf(2L)
        val endDate = LocalDate.of(2020, 10, 1)
        val now = LocalDate.of(2020, 10, 2)

        // when
        val mp = MonthlyProvider(interval, baseEntry, endDate)
        val entries = mutableListOf<MoneyEntry>()
        var tmp: MoneyEntry?
        while(true) {
            tmp = mp.update(now)
            if(tmp == null) break
            entries.add(tmp)
        }

        // then
        assert(entries.size == 2)
        assert(entries[0].date.month == Month.JULY)
        assert(entries[1].date.month == Month.SEPTEMBER)
    }

    @Test
    fun `update method should provide a result for each month expected with changing intervals`() {
        // given
        val baseEntry = MoneyEntry(
            "test desc",
            LocalDate.of(2020, 5, 1),
            Euro(10,0U),
            EntryType.NEED,
            1
        )
        val interval = arrayOf(2L, 1L, 2L)
        val endDate = LocalDate.of(2021, 5, 1)
        val now = LocalDate.of(2021, 5, 2)

        // when
        val mp = MonthlyProvider(interval, baseEntry, endDate)
        val entries = mutableListOf<MoneyEntry>()
        var tmp: MoneyEntry?
        while(true) {
            tmp = mp.update(now)
            if(tmp == null) break
            entries.add(tmp)
        }

        // then
        assert(entries.size == 7)
        assert(entries[0].date.month == Month.JULY) // +2
        assert(entries[1].date.month == Month.AUGUST) // +1
        assert(entries[2].date.month == Month.OCTOBER) // +2
        assert(entries[3].date.month == Month.DECEMBER) // +2
        assert(entries[4].date.month == Month.JANUARY) // +1
        assert(entries[5].date.month == Month.MARCH) // +2
        assert(entries[6].date.month == Month.MAY) // +2
    }

    @Test
    fun `update method should provide a result for each month expected when end is set to null`() {
        // given
        val baseEntry = MoneyEntry(
            "test desc",
            LocalDate.of(2020, 5, 1),
            Euro(10,0U),
            EntryType.NEED,
            1
        )
        val interval = arrayOf(1L)
        val endDate = null
        val now = LocalDate.of(2020, 10, 2)

        // when
        val mp = MonthlyProvider(interval, baseEntry, endDate)
        val entries = mutableListOf<MoneyEntry>()
        var tmp: MoneyEntry?
        while(true) {
            tmp = mp.update(now)
            if(tmp == null) break
            entries.add(tmp)
        }

        // then
        assert(entries.size == 5)
        assert(entries[0].date.month == Month.JUNE)
        assert(entries[1].date.month == Month.JULY)
        assert(entries[2].date.month == Month.AUGUST)
        assert(entries[3].date.month == Month.SEPTEMBER)
        assert(entries[4].date.month == Month.OCTOBER)
    }
}